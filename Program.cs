﻿using System;
using System.Collections.Generic;

namespace delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            List<String> nomes = new List<string>{"Júlio", "Ana", "João", "Márcia", "Pedro"};
            var umnome = nomes.Find(n => n.Length == 3);
            Console.WriteLine(umnome);

            Func<int,int,int> refSomar = Somar;
            Console.WriteLine(refSomar(1,2));

            Func<int,int> refDuplica = x => 2*x;

            nomes.ForEach(n => Console.WriteLine($"nome={n}"));
            */

            Termostato t1 = new Termostato();
            t1.LimiteSuperior = 27.5;

            t1.Alarme += term => {
                Console.WriteLine("Alarme disparado!");
                Console.WriteLine($"Temperaturatura do termostato: {term.TemperaturaAtual}");
            };

            t1.Aumentar(10);
            t1.Aumentar(10);
            t1.Aumentar(10);
            Console.WriteLine(t1.TemperaturaAtual);

        }
        static int Somar(int x, int y)
        {
            return x + y;
        }
    }
}
